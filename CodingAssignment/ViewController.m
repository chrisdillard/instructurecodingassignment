//
//  ViewController.m
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import "ViewController.h"
#import "NetworkingModule.h"
#import "DetailViewController.h"

//
// to be listened to for new photos from n/w module
//
#define PHOTOS_UPDATED @"photosUpdated"

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //handle keyboard
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:UIKeyboardDidShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:UIKeyboardDidHideNotification object:nil];
    
    
    
    //register for networking module updates
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(photosUpdated:)
                                                 name:PHOTOS_UPDATED
                                               object:nil];
    // zero out data source
    photos=[NSArray array];
    
    //create gcd queue for thumbs
    imageQueue = dispatch_queue_create("Image Queue",NULL);

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark handle storyboard segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Make sure your segue name in storyboard is the same as this line
    if ([[segue identifier] isEqualToString:@"showDetailItem"])
    {
        // Get reference to the destination view controller
        DetailViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        [vc setDetailItem:selectedItem];
    }
}


#pragma mark tableview logic

//
// clear the tableview because new data or user cleared
//
-(void) clearAndReloadTableView {

    dispatch_async(dispatch_get_main_queue(), ^{
        [self.tableView reloadData];
    });
}

#pragma mark photos updated

//
// this function is called when n/w module has new data from flickr api call
//
- (void) photosUpdated:(NSNotification *) notification {
    if (notification!=nil){
        NSArray* received = [notification object];
        if (received!=nil){
            
            //update table view data
            photos=received;
            
            //clear photo cache
            photoCache=[NSMutableDictionary dictionary];

            //clear current data and reload table view using photos array
            [self clearAndReloadTableView];
        }
    }
}

#pragma mark UISearchBarDelegate

// PROGRESSIVE LOAD FOR A NOTHER DAY
//- (void)searchBar:(UISearchBar *)searchBar
//    textDidChange:(NSString *)searchText{
//    
//    //clear tableview when search bar zeroed out
//    if ([searchText isEqualToString:@""]){
//        photos = [NSArray array];
//        photoCache=[NSMutableDictionary dictionary];
//        [self clearAndReloadTableView];
//    }
//    // if user has typed something then perform flickr api call
//    else{
//        [[NetworkingModule sharedInstance] flickrSearchWithKeyword:searchText];
//    }
//}


-(void) searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    //clear tableview when search bar zeroed out
    if ([[searchBar text] isEqualToString:@""]){
        photos = [NSArray array];
        photoCache=[NSMutableDictionary dictionary];
        [self clearAndReloadTableView];
    }
    // if user has typed something then perform flickr api call
    else{
        [[NetworkingModule sharedInstance] flickrSearchWithKeyword:[searchBar text]];
    }
    
    [searchBar setText:@""];
    [searchBar resignFirstResponder];
}


#pragma mark UITableViewDelegate
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [photos count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIdentifier = @"photo";

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
    }
    
    //set tableview text to image title
    cell.textLabel.text = [[photos objectAtIndex:indexPath.row] objectForKey:@"title"];

    //construct flickr URL for displaying image
    //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
    NSString* farm = [[photos objectAtIndex:indexPath.row] objectForKey:@"farm"];
    NSString* serverIdentifier = [[photos objectAtIndex:indexPath.row] objectForKey:@"server"];
    NSString* identifier = [[photos objectAtIndex:indexPath.row] objectForKey:@"id"];
    NSString* secret = [[photos objectAtIndex:indexPath.row] objectForKey:@"secret"];
    NSString* constructedUrl =[NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@_t.jpg",farm,serverIdentifier,identifier,secret];
    
    
    //load default image while thumb is downloading
    cell.imageView.image = [UIImage imageNamed:@"loading.png"];
    
    //check if cache has image
    UIImage* cache = [photoCache objectForKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
    if (cache!=nil){
        cell.imageView.image = cache;
    }
    else{
        // dispatch image download from constructed flickr api
        dispatch_async(imageQueue, ^{

            NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:constructedUrl]];

            //now that image is downloaded we can update cache and update tableview row with thumb
            if (imageData) {
                UIImage *image = [UIImage imageWithData:imageData];
                if (image) {
                    
                    //update cache
                    [photoCache setObject:image forKey:[NSString stringWithFormat:@"%ld",(long)indexPath.row]];
                    
                    //run table view update on row that has a thumb downloaded now
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        //update tableview cell image
                        cell.imageView.image = image;
                        
                        //perform update with tableview
                        [self.tableView beginUpdates];
                         [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationNone];
                        [self.tableView endUpdates];

                    });
                }
            }
        });
    }

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    //setup our selected item for segue prepare
    selectedItem=[photos objectAtIndex:indexPath.row];
    //perform segue to detail view controller with this object dict.
    [self performSegueWithIdentifier:@"showDetailItem" sender:self];
}

#pragma mark Handle keyboard
- (void)keyboardDidShow:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    CGRect frame = CGRectMake(self.tableView.frame.origin.x,
                              self.tableView.frame.origin.y,
                              self.tableView.frame.size.width,
                              self.tableView.frame.size.height - size.height);
    self.tableView.frame = frame;
}

- (void)keyboardDidHide:(NSNotification *)notification
{
    NSDictionary *userInfo = [notification userInfo];
    CGSize size = [[userInfo objectForKey: UIKeyboardFrameEndUserInfoKey] CGRectValue].size;
    self.tableView.frame = CGRectMake(self.tableView.frame.origin.x,
                                      self.tableView.frame.origin.y,
                                      self.tableView.frame.size.width,
                                      self.tableView.frame.size.height + size.height);
}

@end
