//
//  NetworkingModule.m
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "NetworkingModule.h"

//
// static flickr api url to have keyword appended
//
#define FLICKRSEARCHAPI @"https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=1dd17dde0fed7286935d83875fcc17dd&per_page=25&format=json&nojsoncallback=1&tags="

#define PHOTOS_UPDATED @"photosUpdated"

@implementation NetworkingModule

//
// setup n/w module as singleton
//
+ (NetworkingModule *)sharedInstance {
    static dispatch_once_t once;
    static NetworkingModule *instance;
    dispatch_once(&once, ^{
        instance = [[NetworkingModule alloc] init];
    });
    return instance;
}

//
// this function performs the flickr search api with keyword or words
// the end result from a succesful api call is "photosUpdated" will be broadcast to notification center
//
-(void) flickrSearchWithKeyword :(NSString*)keyword{
    
    //create nsurlrequest from static flickr url and keyword
    NSString* api = [NSString stringWithFormat:@"%@%@",FLICKRSEARCHAPI,keyword];
    NSURLRequest* req = [NSURLRequest requestWithURL:[NSURL URLWithString:api]];
    
    //create operation queue if first api call
    if (operationQueue==nil)
        operationQueue= [[NSOperationQueue alloc] init];
    
    //run nsurlconnection on network.
    
    [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];

    
    [NSURLConnection sendAsynchronousRequest:req queue:operationQueue completionHandler:^(NSURLResponse *response, NSData *data, NSError *connectionError) {
        if (data!=nil){
            //parse returned data using nsjsonserialization
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            if (json!=nil){
                //get photos array
                NSDictionary* photosNode = [json objectForKey:@"photos"];
                NSArray* photos = [photosNode objectForKey:@"photo"];
                //send result to observers
                [[NSNotificationCenter defaultCenter] postNotificationName:PHOTOS_UPDATED object:photos];
                
                [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];

            }
        }
    }
     ];
    
}

@end