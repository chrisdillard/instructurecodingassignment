//
//  NetworkingModule.h
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//
#import <UIKit/UIKit.h>
#import <Foundation/Foundation.h>
@interface NetworkingModule : NSObject {
    
    //operation queue for network events
    NSOperationQueue* operationQueue;
}

//
// setup n/w module as singleton
//
+ (NetworkingModule*)sharedInstance;

//
// perform flickr search with keyword
//
-(void) flickrSearchWithKeyword :(NSString*)keyword;

@end
