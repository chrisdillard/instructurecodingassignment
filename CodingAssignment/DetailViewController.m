//
//  DetailViewController.m
//  CodingAssignment
//
//  Created by Chris Dillard on 3/6/15.
//  Copyright (c) 2015 Chris. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    imageQueue = dispatch_queue_create("Image Detail Queue",NULL);

    //download image - prevent stretching
    [self.imageView setContentMode:UIViewContentModeScaleAspectFit];
    
    //construct flickr URL for displaying full size image
    //https://farm{farm-id}.staticflickr.com/{server-id}/{id}_{secret}.jpg
    NSString* farm = [self.detailItem objectForKey:@"farm"];
    NSString* serverIdentifier = [self.detailItem objectForKey:@"server"];
    NSString* identifier = [self.detailItem objectForKey:@"id"];
    NSString* secret = [self.detailItem objectForKey:@"secret"];
    NSString* constructedUrl =[NSString stringWithFormat:@"https://farm%@.staticflickr.com/%@/%@_%@.jpg",farm,serverIdentifier,identifier,secret];
    
    //set default image
    self.imageView.image = [UIImage imageNamed:@"loading.png"];
    
    //download image using gcd
    dispatch_async(imageQueue, ^{
        NSData *imageData = [NSData dataWithContentsOfURL:[NSURL URLWithString:constructedUrl]];
        if (imageData) {
            UIImage *image = [UIImage imageWithData:imageData];
            if (image) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    self.imageView.image = image;
                 
                    
                });
            }
        }
    });

    
    //set title
    NSString* title = [self.detailItem objectForKey:@"title"];
    [self setTitle:title];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
